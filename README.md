# Signamed-oakd

Implementation of the Signamed project in the Intel Camera OAK-D. It consists on an automatic sign language recognition prototype, focused in the healthcare sector.


## How does it work?

## Deployment
Two devices are needed in order to run this application, an Intel OAK-D plus a host device, which can be any device capable of running an OS, such as any laptop or a SBC (Raspberry PI, Jetson Nano). Also, if there's a GPU available it's recommended turning the DEPLOYMENT env variable to 'local'; in any other case, if it's set to 'cloud' the host processing will be done remotely.

There are two modes available: dictionary and interpreter. Dictionary records 4 seconds video sequences and then outputs the most likely sign. In the case of the interpreter mode, it is constantly processing the frames with an overlapping factor and then applies a post-processing algorithm in order to extract the most likely sign. The interpreter mode records 700 ms video sequences, so it's intended for more advanced users. Both modes can be triggered with a concrete gesture which can be changed in the environment file, by default, the dictionary mode is triggered pulling out a five with the hands, and the interpreter mode with the peace sign.

### Config OAK-D
The OAK-D must be configured and detected by the host device. The command lsusb should be more than enough to check if the device was correctly detected, but first it must be present in the system udev rules.  

```
echo 'SUBSYSTEM=="usb", ATTRS{idVendor}=="03e7", MODE="0666"' | sudo tee /etc/udev/rules.d/80-movidius.rules
sudo udevadm control --reload-rules && sudo udevadm trigger
lsusb
```

### Run the app
This project already contains all the necessary resources needed in order to build and run this application. The three steps are: installing the requirements, configuring the env file and running the project.

```
# Install requirements
cd src/
pip install -r requirements.txt
# Modify env file
nano .env
# Run project
python run.py
```


## Debug info
The variable DEPTHAI_LEVEL can be set in order to print information about the OAK-D resources
```
DEPTHAI_LEVEL=info python run.py
```
