# Pipeline
Hands tracker pipeline

## Info de recursos
Se se setea a variable de entorno DEPTHAI_LEVEL a info, imprímense os recursos na consola.
```
DEPTHAI_LEVEL=info python run.py
```
## XLinks
Funcionan creando o link de saída, e enchufándolles unha entrada (que define o que vai saír). Algo lío pero moi sinxelo. Estructura: mateo.out(alicia.in), quere dicir que mateo entra en alicia.

* RGB Camera out
* Stereo r/l entran no bloque StereoDepth
* Spatial location calculator saca info xyz
* Body:
    * Pre: toma como entrada o RGB e pilla a config do manager script
    * NN: entrada é o anterior pto, e saída vai cara ao manager script (from_body_nn)
* Palm detection:
    * Pre: toma como entrada o RGB e pilla a config do manager script
    * NN: entrada é o anterior pto
    * Post NN: entrada é o anterior pto, e saída vai cara ao manager script (from_post_dd_nn), e deste vai cara ao HOST -> NON se usa!
* Hand landmark:
    * Pre: toma como entrada a palma da man e pilla a config do manager script
    * NN: entrada é o anterior pto, e saída vai cara ao manager script (from_lm_nn)

## Queues
* Cam out: imaxe RGB
* Manager out: landmarks e saídas da NN

## Scripts
* Manager script: manexa procesados das NN

# Models
## Movenet body

* nose: 0,
* left_eye: 1,
* right_eye: 2,
* left_ear: 3,
* right_ear: 4,
* left_shoulder: 5,
* right_shoulder: 6,
* left_elbow: 7,
* right_elbow: 8,
* left_wrist: 9,
* right_wrist: 10,

## Hand landmarks
* wrist: 0