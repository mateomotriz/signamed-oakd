import sys,json
sys.path.insert(0, './service/computeService/msg3d')
sys.path.insert(0, './service/computeService')
from dotenv import load_dotenv
from service.predictionService.predictionService import PredictionService
from service.cloudService.cloudService import CloudService
import numpy as np
from service.computeService.preprocessing.src.gen_features import mediapipe

# Load env variables
load_dotenv()

nMODEL = 20


for i in range(2):
        mediapipe['max_frame'] = nMODEL
        print ('Max frame: {}'.format(mediapipe['max_frame']))

        predictionService = CloudService()
        predictionService.load_model(nMODEL)

        with open(sys.argv[1], "r") as f:
                data = json.loads(f.read())

        data = np.array(data)

        predictionService.request_sign(data, False)
        # predictionService.request_sign(data[0:20], False)
        # predictionService.request_sign(data[10:30], False)
        # predictionService.request_sign(data[30:], False)

        nMODEL = 120


