import numpy as np
from collections import Counter
from scipy import signal


class DataService:

    # CONSTs
    N_KEYPOINTS = 53
    N_PARAMS = 4

    # VARs
    kpsArray = np.zeros([N_KEYPOINTS, N_PARAMS])
    framesArray = None

    def __init__(self) -> None:
        pass

    # FRAME & KPS
    def parse_kps(self, hands, body):
        # Init array of zeros
        kpsAuxArray = self.kpsArray.copy()

        # Body, z and esperanza are both 1
        if body is not None:
            kpsAuxArray[0:11, 0:2] = body.keypoints_norm[0:11, :]
            kpsAuxArray[0:11, 2:4] = 1

        # Hand, we do not have the 'esperanza', we set it to 1
        for hand in hands:
            if (hand.label == 'right'):
                kpsAuxArray[32:, 0:3] = hand.norm_landmarks
                kpsAuxArray[32:, 3:4] = 1

                # DEBUG
                # print(
                #     "MIXED-WORLD", round(hand.xyz_mixed[4, 2], 3), round(hand.world_landmarks[4, 2], 3))

            elif (hand.label == 'left'):
                kpsAuxArray[11:32, 0:3] = hand.norm_landmarks
                kpsAuxArray[11:32, 3:4] = 1

        # First detection
        if self.framesArray is not None:
            kpsAuxArray = np.expand_dims(kpsAuxArray, axis=0)
            self.framesArray = np.vstack((self.framesArray, kpsAuxArray))
        # Stack vertically
        else:
            self.framesArray = np.array(kpsAuxArray)
            self.framesArray = np.expand_dims(self.framesArray, axis=0)

        return self.framesArray

    def rearrange_frames(self, cutWindow):
        self.framesArray = self.framesArray[cutWindow:]

    def reset_frames(self):
        self.framesArray = None

    def resample_signal(self, frames, t, nframes):
        self.framesArray = np.asarray(signal.resample(frames, nframes, np.asarray(t), axis=0)[0])
        return self.framesArray

    # UTILs
    # Checks if a value is repeated more than 'threshold' time
    def check_window(self, array, threshold):
        for item, count in Counter(array).items():
            if count >= threshold:
                return item
        return ''

    # Pushes a new value to a fixed size array
    def rotate_and_addvalue(self, array, value):
        array = np.roll(array, 1)
        array[0] = value
        return array
