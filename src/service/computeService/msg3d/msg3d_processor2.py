#!/usr/bin/env python
import os, sys
import yaml
import pickle
import argparse
from collections import OrderedDict, defaultdict
import torch
from torchsummary import summary
import numpy as np
import torch.nn as nn
import torch.nn.functional as F


def import_class(name):
    components = name.split('.')
    mod = __import__(components[0])
    for comp in components[1:]:
        mod = getattr(mod, comp)
    return mod


def flip(data_numpy):

    flipped_data = np.copy(data_numpy)
    flipped_data[0, :, :, :] *= -1

    return flipped_data

# AUX class to convert dict into Namespaces
class Bunch(object):
  def __init__(self, adict):
    self.__dict__.update(adict)

class MSG3D_Processor():
    MODEL_ARGS = os.getenv('MODEL_ARGS')
    CONFIG_MODEL = os.getenv('LSE_CONFIG_MODEL')
    WEIGHTS = os.getenv('LSE_WEIGHTS')
    """Processor for Skeleton-based Action Recgnition"""

    def __init__(self, config):
        with open(config, 'r') as f:
            self.args = Bunch(yaml.load(f))
        self.args.device = int(os.getenv('LSE_DEVICE'))
        #print("ARGS", self.args.model_args)
        
        self.load_model()
        self.model.eval()

        if type(self.args.device) is list:
            if len(self.args.device) > 1:
                # self.print_log(
                #     f'{len(self.args.device)} GPUs available, using DataParallel')
                self.model = nn.DataParallel(
                    self.model,
                    device_ids=self.args.device,
                    output_device=self.output_device
                )

    def load_model(self):
        output_device = self.args.device[0] if type(
            self.args.device) is list else self.args.device
        self.output_device = output_device
        Model = import_class(self.args.model)

        self.model = Model(**self.args.model_args).cuda(output_device)
        self.loss = nn.CrossEntropyLoss().cuda(output_device)

        if self.args.weights:
            try:
                self.global_step = int(self.args.weights[:-3].split('-')[-1])
            except:
                #print('Cannot parse global_step from model weights filename')
                self.global_step = 0

            #print('Loading weights from: {}'.format(self.args.weights))
            if '.pkl' in self.args.weights:
                with open(self.args.weights, 'r') as f:
                    weights = pickle.load(f)
            else:
                weights = torch.load(self.args.weights)

            weights = OrderedDict(
                [[k.split('module.')[-1],
                  v.cuda(output_device)] for k, v in weights.items()])

            try:
                self.model.load_state_dict(weights)
            except:
                state = self.model.state_dict()
                diff = list(set(state.keys()).difference(set(weights.keys())))
                #print('Can not find these weights:')
                # for d in diff:
                #     self.print_log('  ' + d)
                state.update(weights)
                self.model.load_state_dict(state)

        for param in self.model.parameters():
            param.requires_grad = False

    def inference(self, data):
        data_flip = np.copy(data)
        data_flip = flip(data_flip)

        data = torch.from_numpy(np.array([data]))
        data_flip = torch.from_numpy(np.array([data_flip]))

        self.model = self.model.cuda(self.output_device)
        data = data.float().cuda(self.output_device)
        data_flip = data_flip.float().cuda(self.output_device)

        # #print ('data_shape: {}', data.shape)
        # #print ('data_flipped_shape: {}', data_flip.shape)
        #summary(self.model, (3, 157, 51, 1))

        output = self.model(data)

        # Disabled use of flipped and combinate them. Use only normal data.
        #output_flipped = output
        #output_tta = output
        output_flipped = self.model(data_flip)
        output_tta = (output + output_flipped) / 2

        output_softmax = F.softmax(output_tta[0], dim=0)
        output_softmax = output_softmax.reshape(1, len(output_softmax))

        prob, indices = output_softmax.topk(3)

        return indices.cpu().numpy()[0], prob.cpu().numpy()[0], output_tta, output, output_flipped

    def inference_softmax(self, data):
        data_flip = np.copy(data)
        data_flip = flip(data_flip)

        data = torch.from_numpy(np.array([data]))
        data_flip = torch.from_numpy(np.array([data_flip]))

        self.model = self.model.cuda(self.output_device)
        data = data.float().cuda(self.output_device)
        data_flip = data_flip.float().cuda(self.output_device)

        # #print ('data_shape: {}', data.shape)
        # #print ('data_flipped_shape: {}', data_flip.shape)
        #summary(self.model, (3, 157, 51, 1))

        output = self.model(data)

        # Disabled use of flipped and combinate them. Use only normal data.
        #output_flipped = output
        #output_tta = output
        output_flipped = self.model(data_flip)
        output_tta = (output + output_flipped) / 2

        output_softmax = F.softmax(output_tta[0], dim=0)
        output_softmax = output_softmax.reshape(1, len(output_softmax))

        prob, indices = output_softmax.topk(82)

        return indices.cpu().numpy()[0], prob.cpu().numpy()[0], output_softmax

    def inference_softmax_with_batch_size(self, data):
        if data.ndim == 4:
            data = np.array([data])

        data_flip = np.copy(data)
        data_flip = flip(data_flip)

        data = torch.from_numpy(np.array(data))
        data_flip = torch.from_numpy(np.array(data_flip))

        self.model = self.model.cuda(self.output_device)
        data = data.float().cuda(self.output_device)
        data_flip = data_flip.float().cuda(self.output_device)
        output = self.model(data)

        output_flipped = self.model(data_flip)
        output_tta = (output + output_flipped) / 2

        arr_output_softmax = []
        arr_prob = []
        arr_indices = []
        for out_idx in range(data.shape[0]):
            output_softmax = F.softmax(output_tta[out_idx], dim=0)
            output_softmax = output_softmax.reshape(1, len(output_softmax))
            prob, indices = output_softmax.topk(82)

            arr_output_softmax.append(output_softmax)
            arr_indices.append(indices.cpu().numpy()[0])
            arr_prob.append(prob.cpu().numpy()[0])

        return arr_indices, arr_prob, arr_output_softmax