import os
import re
import time
import requests


class CloudService:
    # GUESSED SIGN
    lse_sign = '¿?'

    # URL params
    URL = None
    PAYLOAD = {'user_id': 'uidxxxxx'}

    def __init__(self):
        self.URL = f'{os.getenv("SIGNAMED_URL")}:{os.getenv("SIGNAMED_PORT")}/sendMediapipe'

    # REQUEST
    def request_sign(self, results, checkProbability, initialTime):
        # Save 2 file
        self._save_file(results)
        FILES = [('mediapipe', ('result.json', open(
            '.tmp/result.json', 'rb'), 'application/json'))]

        # Process request
        res = requests.request(
            "POST", self.URL, data=self.PAYLOAD, files=FILES, verify=False)
        response = res.json()

        print("SIGN: ", response['signs'][0],
              "with probabilities", response['probabilities'][0])

        if (not(checkProbability) or float(response['probabilities'][0]) > float(os.getenv('SIGN_THRESHOLD'))):
            self.lse_sign = response['signs'][0]

        print("Elapsed time:", time.time() - initialTime, res.elapsed.total_seconds())

    def get_sign(self):
        return self.lse_sign

    # Dummy methods (to match the GPU options)
    def get_current_model(self):
        return 120

    def load_model(self, model):
        pass

    def set_sign(self, sign):
        self.lseSign = sign

    # UTILs

    def _save_file(self, results):
        f = open(".tmp/result.json", "w")
        f.write(str(results.tolist()))
        f.close()
