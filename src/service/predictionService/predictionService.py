from service.computeService.preprocessing.src.gen_features import mediapipe
from service.computeService.msg3d.msg3d_processor2 import MSG3D_Processor
from service.computeService.preprocessing.src.gen_features import GenFeaturesMediapipeC4 as Features
from service.computeService.preprocessing.src.gen_features import MediapipeOptions
from dotenv import load_dotenv
import torch.nn.functional as F
from service.predictionService.configs import Config
import json
import numpy as np
import os
import sys
import time
sys.path.insert(0, './service/computeService/msg3d')
sys.path.insert(0, './service/computeService')


class PredictionService:
    # Vars
    genFeatures = None
    configs = None
    inputs = []
    models = []
    signs_dict = {}
    lse_sign = ''
    current_model = 0
    data_service = None

    def __init__(self, data_service):
        self.genFeatures = Features(MediapipeOptions.XYZ)
        self.get_signs_dict()
        self.data_service = data_service

        # Env
        # Postpro
        self.signsArray = np.array(
            int(os.getenv('SIGN_WINDOW_SIZE')) * [''],  dtype=object)
        self.signsArrayNumber = int(os.getenv('SIGN_WINDOW_NUMBER'))

    def get_signs_dict(self):
        i = 0
        with open('./signamed.txt') as f:
            content = f.readlines()
            for line in content:
                self.signs_dict[i] = line.replace('\n', '')
                i += 1

    def load_model(self, model):
        self.current_model = model
        mediapipe['max_frame'] = model
        model_path = os.getenv('LSE_CONFIG_MODEL')
        config_model = f'{model_path}_{model}.yaml'
        self.configs = Config(config_model)

        for config_msg3d_idx in self.configs.msg3d_configs_file:
            self.inputs.append(config_msg3d_idx[0])
            self.models.append(MSG3D_Processor(config_msg3d_idx[1]))

        # Launch 1 detection to load init parameters
        with open('../examples/json/temp_afectar.json', "r") as f:
            data = json.loads(f.read())
            self.request_sign(data, True, time.time(), False)

    def get_current_model(self):
        return self.current_model

    def request_sign(self, data, checkProbability, initialTime, setSign=True):
        # with open(input, "r") as f:
        #     data = json.loads(f.read())
        keypoints = np.asarray(data, dtype=np.float64)
        data_joints, data_bones, data_motion_joints, data_motion_bones = self.genFeatures.getFeatures(
            keypoints)
        inputs_features = {
            'data_joints': data_joints,
            'data_bones': data_bones,
            'data_motion_joints': data_motion_joints,
            'data_motion_bones': data_motion_bones
        }

        output_ensemble = []
        for idx in range(len(self.inputs)):
            index, prob, output, output_normal, output_flipped = self.models[idx].inference(
                inputs_features[self.inputs[idx]])

            if idx == 0:
                output_ensemble = output_normal
            else:
                output_ensemble = (output_ensemble + output_normal) / 2

        output_ensemble = F.softmax(output_ensemble[0], dim=0)
        output_ensemble = output_ensemble.reshape(1, len(output_ensemble))
        prob, index = output_ensemble.topk(3)

        index_list = index.tolist()[0]
        prob_list = prob.tolist()[0]
        decoded_sign = self.signs_dict[index_list[0]]

        if setSign:
            self.signsArray = self.data_service.rotate_and_addvalue(
                self.signsArray, decoded_sign)

            if (not(checkProbability) or float(prob_list[0]) > float(os.getenv('SIGN_THRESHOLD'))):
                self.lse_sign = decoded_sign

                print("- PROB SIGN: ", decoded_sign, "with probabilities",
                      prob_list[0], "Elapsed time:", time.time() - initialTime)
            else:
                repeated_sign = self.data_service.check_window(
                    self.signsArray, self.signsArrayNumber)
                if repeated_sign != '':
                    self.lse_sign = decoded_sign
                    print("- ARRAY SIGN: ", decoded_sign, "with probabilities",
                          prob_list[0], "Elapsed time:", time.time() - initialTime)
                    # print("SIGNS ARRAY:", self.signsArray)

    def get_sign(self):
        return self.lse_sign
