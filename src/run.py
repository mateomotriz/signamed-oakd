#!/usr/bin/env python3

import os
import json
import time
from service.predictionService.predictionService import PredictionService
from trackers.handsBodyTrackerRenderer import HandsBodyTrackerRenderer
from trackers.handsBodyTrackerEdge import HandsBodyTracker
from service.cloudService.cloudService import CloudService
from service.dataService.dataService import DataService
import threading
import numpy as np
from dotenv import load_dotenv

# Load env variables
load_dotenv()

# SHOW CAMERA
SHOW_CAMERA = True if os.getenv('SHOW_CAMERA') == 'True' else False

# Frame window constants
FRAME_WINDOW_TIME = float(os.getenv('FRAME_WINDOW_TIME'))
FRAME_WINDOW_OVERLAP_INV = FRAME_WINDOW_TIME - \
    float(os.getenv('FRAME_WINDOW_OVERLAP')) * FRAME_WINDOW_TIME

# VARs
signingTimestamp = generalTimestamp = 0
signingTimeArray = []
signingGestureArray = np.array(30 * [''],  dtype=object)
signingStateDict = {'IDLE': 'IDLE', 'DICT': 'DICTIONARY', 'INT': 'INTERPRETER'}
signingState = signingStateDict['IDLE']

if __name__ == '__main__':

    # 1. INIT OBJECTS

    dataService = DataService()

    signService = PredictionService(dataService) if os.getenv(
        'DEPLOYMENT') == 'local' else CloudService()

    tracker = HandsBodyTracker(
        input_src='rgb',
        use_world_landmarks=True if os.getenv(
            'WORLD_LANDMARKS') == 'True' else False,
        xyz=True if os.getenv('XYZ') == 'True' else False,
        crop=True if os.getenv('CROP_IMAGE') == 'True' else False,
        resolution=os.getenv('RESOLUTION'),
        stats=True if os.getenv('STATS') == 'True' else False,
        trace=int(os.getenv('DEBUG_LEVEL')),
        use_handedness_average=True if os.getenv(
            'LAST_HANDENESS') == 'True' else False,
        use_gesture=True if os.getenv(
            'USE_GESTURE') == 'True' else False,
        single_hand_tolerance_thresh=int(os.getenv('SINGLE_HAND_THRESHOLD')),
        lm_nb_threads=int(os.getenv('LM_THREADS')),
        body_model=os.getenv('BODY_MODEL'))

    renderer = HandsBodyTrackerRenderer(
        tracker=tracker) if SHOW_CAMERA else None

    # 2. START PARSING KEYPOINTS

    while True:
        # Count total frames & Run hand tracker on next frame
        frame, hands, body = tracker.next_frame(use_hand_wrist=True)
        if frame is None:
            break

        # 3. CHOOSE MODE
        # IDLE (waiting for start gesture)
        if signingState == signingStateDict['IDLE']:
            if len(hands) != 0 and hands[0].gesture is not None:
                # Rotate array and add new value
                signingGestureArray = dataService.rotate_and_addvalue(
                    signingGestureArray, hands[0].gesture)
                filteredGesture = dataService.check_window(
                    signingGestureArray, 30).lower()

                if filteredGesture == os.getenv("DICT_SIGN"):
                    # Update state and clear array
                    signingState = signingStateDict['DICT']
                    signingGestureArray = np.array(30 * [''],  dtype=object)

                    # Change model in local mode
                    if (os.getenv('DEPLOYMENT') == 'local' and signService.get_current_model() != 120):
                        print("Loading 120 FPS model")
                        signService.load_model(120)
                        print("120 frames model loaded")
                    else:
                        # Wait 2s for preparation in cloud mode or if the model is compiled
                        print("DICT: Starting in 3 s", signingState)
                        time.sleep(3)
                elif filteredGesture == os.getenv("INTERPRETER_SIGN"):
                    # Update state and clear array
                    signingState = signingStateDict['INT']
                    signingGestureArray = np.array(30 * [''],  dtype=object)

                    # Change model in local mode
                    if (os.getenv('DEPLOYMENT') == 'local' and signService.get_current_model() != 20):
                        print("Loading 20 FPS model")
                        signService.load_model(20)
                        print("20 frames model loaded...")
                    elif (os.getenv('DEPLOYMENT') == 'cloud'):
                        print("Interpreter mode only available in local (GPU) mode")
                        signingState = signingStateDict['IDLE']
                    else:
                        # Start right on
                        print("INTERPRETER: Starting right on")

                # Save timestamp
                signingTimestamp = generalTimestamp = time.time()
                signingTimeArray = []

            else:
                signingGestureArray = dataService.rotate_and_addvalue(
                    signingGestureArray, '')

        # DICT: Make one petition, reset frames and go back to IDLE
        else:
            # Parse keypints
            framesArray = dataService.parse_kps(hands, body)
            frameTimestamp = time.time() - generalTimestamp
            signingTimeArray.append(frameTimestamp)

            if signingState == signingStateDict['DICT']:
                # If processing is complete makes the petition (without checking probabilities)
                if frameTimestamp >= 4:
                    # Pre-processing
                    resampledFramesArray = dataService.resample_signal(
                        framesArray, signingTimeArray, signService.get_current_model())

                    # Petition
                    x = threading.Thread(
                        target=signService.request_sign, args=(resampledFramesArray, False, time.time())).start()

                    # Post-processing
                    dataService.reset_frames()
                    signingState = signingStateDict['IDLE']

            # INTERPRETER: Keep making petitions
            elif signingState == signingStateDict['INT']:
                # Frame window sender (2 conditions)
                # A. The first time waits until the actual time needed for the detection
                # B. The subsequent detections need less time because they reuse frames
                if time.time() - generalTimestamp > FRAME_WINDOW_TIME and (time.time() - signingTimestamp) > FRAME_WINDOW_OVERLAP_INV:
                    # Pre-processing
                    signingTimestamp = time.time()

                    try:
                        resampledFramesArray = dataService.resample_signal(
                            framesArray, signingTimeArray[len(signingTimeArray)-framesArray.shape[0]:], signService.get_current_model())
                    except:
                        resampledFramesArray = framesArray

                    # Request sign in backend
                    x = threading.Thread(
                        target=signService.request_sign, args=(resampledFramesArray, True, time.time())).start()

                    # Post-processing
                    dataService.rearrange_frames(
                        int(FRAME_WINDOW_OVERLAP_INV * signService.get_current_model()))

        # UTILs
        if SHOW_CAMERA:
            frame = renderer.draw(frame, hands, body,
                                  signService.get_sign(), signingState)

            # Exit if 'q' is pressed
            key = renderer.waitKey(delay=1)
            if (key == 27 or key == ord("q")):
                break

    renderer.exit()
    tracker.exit()
