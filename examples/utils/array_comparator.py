import json
import sys
import numpy as np
from matplotlib import pyplot as plt

# DEF

BODY_KP = {
    'nose': 0,
    'left_eye': 1,
    'right_eye': 2,
    'left_ear': 3,
    'right_ear': 4,
    'left_shoulder': 5,
    'right_shoulder': 6,
    'left_elbow': 7,
    'right_elbow': 8,
    'left_wrist': 9,
    'right_wrist': 10,
    'left_hip': 11,
    'right_hip': 12,
    'left_knee': 13,
    'right_knee': 14,
    'left_ankle': 15,
    'right_ankle': 16
}

KPS_SHOW = [31]

def add_arrow(line, position=None, direction='right', size=15, color=None):
    if color is None:
        color = line.get_color()

    xdata = line.get_xdata()
    ydata = line.get_ydata()

    # find closest index
    start_ind = 8
    if direction == 'right':
        end_ind = start_ind + 1
    else:
        end_ind = start_ind - 1

    line.axes.annotate('',
                       xytext=(xdata[start_ind], ydata[start_ind]),
                       xy=(xdata[end_ind], ydata[end_ind]),
                       arrowprops=dict(arrowstyle="->", color=color),
                       size=size
                       )


# MAIN
if __name__ == '__main__':
    # JSON file
    f = open(sys.argv[1], "r")
    fb = open(sys.argv[2], "r")

    # Reading from file
    a = np.array(json.loads(f.read()))
    b = np.array(json.loads(fb.read()))
    f.close()

    # Parse arrays
    #for i in range(min(a.shape[0], b.shape[0])):
    #    print(f'{i}: {a[i, KPS_SHOW, 0:2]} - {b[i, KPS_SHOW, 0:2]}')
    print(f'Dim a: {a.shape[0]} - Dim b: {b.shape[0]}')

    # Plots
    plots = []
    plots.append(plt.plot(a[:, KPS_SHOW, 0], a[:, KPS_SHOW, 1],
                 color='g', label=sys.argv[1])[0])
    plots.append(plt.plot(b[:, KPS_SHOW, 0], b[:, KPS_SHOW, 1],
                 color='r', label=sys.argv[2])[0])

    # Limits & Coordinates
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.gca().invert_yaxis()

    # Add arrows & Show
    for p in plots:
        add_arrow(line=p, color='#FF0000', size=20)
    
    plt.legend()
    plt.show()
