import json
import sys
import numpy as np
from scipy import signal

# MAIN
if __name__ == '__main__':
    # JSON file
    f = open(sys.argv[1], "r")
    a = np.array(json.loads(f.read()))
    f.close()

    # Resample, outputs (x_new, t_new)
    t = np.linspace(0, 4, a.shape[0], endpoint=False)
    b = np.asarray(signal.resample(a, 120, t, axis=0)[0])
    print(f'Prev dim: {a.shape} ~ New dim: {b.shape}')
    c = np.asarray(signal.decimate(a, 20, axis=0))
    print(f'Prev dim: {a.shape} ~ New dim: {c.shape}')

    # Write to file
    f = open("resample.json", "w")
    f.write(str(b.tolist()))
    f.close()
