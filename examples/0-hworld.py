import numpy as np
import cv2
import depthai

# Mandatory, contains all the tools
pipeline = depthai.Pipeline()

# Initiate camera
cam_rgb = pipeline.createColorCamera()
cam_rgb.setPreviewSize(300, 300)
cam_rgb.setInterleaved(False)

# Load model/neural network
detection_nn = pipeline.createNeuralNetwork()
detection_nn.setBlobPath("./models/mobilenet-ssd.blob")

# Show camera
cam_rgb.preview.link(detection_nn.input)

# XLink communicates device and host
xout_rgb = pipeline.createXLinkOut()
xout_rgb.setStreamName("rgb")
cam_rgb.preview.link(xout_rgb.input)

xout_nn = pipeline.createXLinkOut()
xout_nn.setStreamName("nn")
detection_nn.out.link(xout_nn.input)

# Initiate device
device = depthai.Device(pipeline)
device.startPipeline()

# Helpers
q_rgb = device.getOutputQueue("rgb")
q_nn = device.getOutputQueue("nn")

frame = None
bboxes = []


# Convert bounding box float values to pixels
def frame_norm(frame, bbox):
    return (
        np.array(bbox) * np.array([*frame.shape[:2], *frame.shape[:2]])[::-1]
    ).astype(int)


while True:
    # Fetch results
    in_rgb = q_rgb.tryGet()
    in_nn = q_nn.tryGet()

    if in_rgb is not None:
        shape = (3, in_rgb.getHeight(), in_rgb.getWidth())
        frame = in_rgb.getData().reshape(shape).transpose(1, 2, 0).astype(np.uint8)
        frame = np.ascontiguousarray(frame)

    if in_nn is not None:
        bboxes = np.array(in_nn.getFirstLayerFp16())
        # Remove trailing 0s
        bboxes = bboxes[: np.where(bboxes == -1)[0][0]]
        # Reshape into 2D
        bboxes = bboxes.reshape((bboxes.size // 7, 7))
        # Filter by threshold
        bboxes = bboxes[bboxes[:, 2] > 0.8][:, 3:7]

    # Display results
    if frame is not None:
        for raw_bbox in bboxes:
            bbox = frame_norm(frame, raw_bbox)
            cv2.rectangle(frame, (bbox[0], bbox[1]), (bbox[2], bbox[3]), (255, 0, 0), 2)
        cv2.imshow("preview", frame)

    if cv2.waitKey(1) == ord("q"):
        break
